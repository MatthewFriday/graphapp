﻿namespace Gfx
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.canvas = new System.Windows.Forms.Panel();
            this.labelQuality = new System.Windows.Forms.Label();
            this.selectQuality = new System.Windows.Forms.ComboBox();
            this.selectGraph = new System.Windows.Forms.ComboBox();
            this.labelGraph = new System.Windows.Forms.Label();
            this.labelCursor = new System.Windows.Forms.Label();
            this.devMode = new System.Windows.Forms.CheckBox();
            this.offScreenRender = new System.Windows.Forms.CheckBox();
            this.connectPoints = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // canvas
            // 
            this.canvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.canvas.BackColor = System.Drawing.Color.White;
            this.canvas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canvas.Location = new System.Drawing.Point(12, 40);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(680, 281);
            this.canvas.TabIndex = 0;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            this.canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseDown);
            this.canvas.MouseEnter += new System.EventHandler(this.canvas_MouseEnter);
            this.canvas.MouseLeave += new System.EventHandler(this.canvas_MouseLeave);
            this.canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseMove);
            this.canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseUp);
            // 
            // labelQuality
            // 
            this.labelQuality.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelQuality.AutoSize = true;
            this.labelQuality.Location = new System.Drawing.Point(481, 14);
            this.labelQuality.Name = "labelQuality";
            this.labelQuality.Size = new System.Drawing.Size(86, 15);
            this.labelQuality.TabIndex = 1;
            this.labelQuality.Text = "Render quality:";
            // 
            // selectQuality
            // 
            this.selectQuality.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectQuality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectQuality.FormattingEnabled = true;
            this.selectQuality.Location = new System.Drawing.Point(573, 11);
            this.selectQuality.Name = "selectQuality";
            this.selectQuality.Size = new System.Drawing.Size(119, 23);
            this.selectQuality.TabIndex = 2;
            this.selectQuality.SelectedIndexChanged += new System.EventHandler(this.selectQuality_SelectedIndexChanged);
            // 
            // selectGraph
            // 
            this.selectGraph.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectGraph.FormattingEnabled = true;
            this.selectGraph.Items.AddRange(new object[] {
            "Exponenciális egyenlet",
            "Logaritmusos egyenlet",
            "Másodfokú egyenlet",
            "Harmadfokú egyenlet"});
            this.selectGraph.Location = new System.Drawing.Point(12, 11);
            this.selectGraph.Name = "selectGraph";
            this.selectGraph.Size = new System.Drawing.Size(183, 23);
            this.selectGraph.TabIndex = 3;
            this.selectGraph.SelectedIndexChanged += new System.EventHandler(this.selectGraph_SelectedIndexChanged);
            // 
            // labelGraph
            // 
            this.labelGraph.AutoSize = true;
            this.labelGraph.Location = new System.Drawing.Point(201, 14);
            this.labelGraph.Name = "labelGraph";
            this.labelGraph.Size = new System.Drawing.Size(39, 15);
            this.labelGraph.TabIndex = 4;
            this.labelGraph.Text = "f(x) = ";
            // 
            // labelCursor
            // 
            this.labelCursor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCursor.AutoSize = true;
            this.labelCursor.Location = new System.Drawing.Point(12, 324);
            this.labelCursor.Name = "labelCursor";
            this.labelCursor.Size = new System.Drawing.Size(48, 15);
            this.labelCursor.TabIndex = 5;
            this.labelCursor.Text = "Cursor: ";
            // 
            // devMode
            // 
            this.devMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.devMode.AutoSize = true;
            this.devMode.Location = new System.Drawing.Point(597, 323);
            this.devMode.Name = "devMode";
            this.devMode.Size = new System.Drawing.Size(95, 19);
            this.devMode.TabIndex = 6;
            this.devMode.Text = "Debug mode";
            this.devMode.UseVisualStyleBackColor = true;
            // 
            // offScreenRender
            // 
            this.offScreenRender.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.offScreenRender.AutoSize = true;
            this.offScreenRender.Location = new System.Drawing.Point(471, 323);
            this.offScreenRender.Name = "offScreenRender";
            this.offScreenRender.Size = new System.Drawing.Size(120, 19);
            this.offScreenRender.TabIndex = 7;
            this.offScreenRender.Text = "Render off-screen";
            this.offScreenRender.UseVisualStyleBackColor = true;
            this.offScreenRender.CheckedChanged += new System.EventHandler(this.offScreenRender_CheckedChanged);
            // 
            // connectPoints
            // 
            this.connectPoints.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.connectPoints.AutoSize = true;
            this.connectPoints.Checked = true;
            this.connectPoints.CheckState = System.Windows.Forms.CheckState.Checked;
            this.connectPoints.Location = new System.Drawing.Point(358, 323);
            this.connectPoints.Name = "connectPoints";
            this.connectPoints.Size = new System.Drawing.Size(107, 19);
            this.connectPoints.TabIndex = 8;
            this.connectPoints.Text = "Connect points";
            this.connectPoints.UseVisualStyleBackColor = true;
            this.connectPoints.CheckedChanged += new System.EventHandler(this.connectPoints_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 341);
            this.Controls.Add(this.connectPoints);
            this.Controls.Add(this.offScreenRender);
            this.Controls.Add(this.devMode);
            this.Controls.Add(this.labelCursor);
            this.Controls.Add(this.labelGraph);
            this.Controls.Add(this.selectGraph);
            this.Controls.Add(this.selectQuality);
            this.Controls.Add(this.labelQuality);
            this.Controls.Add(this.canvas);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gfx";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel canvas;
        private System.Windows.Forms.Label labelQuality;
        private System.Windows.Forms.ComboBox selectQuality;
        private System.Windows.Forms.ComboBox selectGraph;
        private System.Windows.Forms.Label labelGraph;
        private System.Windows.Forms.Label labelCursor;
        private System.Windows.Forms.CheckBox devMode;
        private System.Windows.Forms.CheckBox offScreenRender;
        private System.Windows.Forms.CheckBox connectPoints;
    }
}

