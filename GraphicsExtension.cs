﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Text;

namespace Gfx
{
    public static class GraphicsExtension
    {
        public static void SetGraphicsQuality(this Graphics gfx, GraphicsQuality quality)
        {
            switch (quality)
            {
                case GraphicsQuality.Low:
                    gfx.SmoothingMode = SmoothingMode.None;
                    gfx.InterpolationMode = InterpolationMode.NearestNeighbor;
                    gfx.PixelOffsetMode = PixelOffsetMode.None;
                    gfx.CompositingQuality = CompositingQuality.HighSpeed;
                    gfx.TextRenderingHint = TextRenderingHint.SingleBitPerPixel;
                    break;
                case GraphicsQuality.Normal:
                    gfx.SmoothingMode = SmoothingMode.HighQuality;
                    gfx.InterpolationMode = InterpolationMode.Bilinear;
                    gfx.PixelOffsetMode = PixelOffsetMode.None;
                    gfx.CompositingQuality = CompositingQuality.HighSpeed;
                    gfx.TextRenderingHint = TextRenderingHint.SystemDefault;
                    break;
                case GraphicsQuality.Maximum:
                    gfx.SmoothingMode = SmoothingMode.HighQuality;
                    gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    gfx.CompositingQuality = CompositingQuality.HighQuality;
                    gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                    break;
            }
        }

        public static PointF ToPointF(this Point o) => new PointF(o.X, o.Y);
        public static Point ToPoint(this PointF o) => new Point((int)o.X, (int)o.Y);
        public static bool IsBetween(this PointF o, PointF start, PointF end)
        {
            return new RectangleF(Math.Min(start.X, end.X),
               Math.Min(start.Y, end.Y),
               Math.Abs(start.X - end.X),
               Math.Abs(start.Y - end.Y)).Contains(o);
        }

        public static RectangleF CreateRectangle(this PointF start, PointF end) => new RectangleF(Math.Min(start.X, end.X),
               Math.Min(start.Y, end.Y),
               Math.Abs(start.X - end.X),
               Math.Abs(start.Y - end.Y));
    }

    public enum GraphicsQuality
    {
        Low,
        Normal,
        Maximum
    }
}
