﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Gfx
{
    public partial class GraphEditor : Form
    {
        private float? A = null;
        private float? B = null;
        private float? C = null;
        private float? D = null;

        private int baseWidth = 0;

        private GraphType Type;

        public GraphEditor(GraphType type)
        {
            this.Type = type;
            InitializeComponent();

            this.baseWidth = this.Width;

            switch (type)
            {
                case GraphType.Exponencial:
                case GraphType.Logarithmic:
                    inputA.Enabled = true;
                    inputB.Enabled = false;
                    inputC.Enabled = false;
                    inputD.Enabled = false;
                    break;
                case GraphType.Quadratic:
                    inputA.Enabled = true;
                    inputB.Enabled = true;
                    inputC.Enabled = true;
                    inputD.Enabled = false;
                    break;
                case GraphType.Cubic:
                    inputA.Enabled = true;
                    inputB.Enabled = true;
                    inputC.Enabled = true;
                    inputD.Enabled = true;
                    break;

                default:
                    this.Close();
                    break;
            }

            labelGraph.Text = "f(x) = " + Graph.ToString(Type, A, B, C, D);
        }

        private void ValidateInput(object sender, EventArgs e)
        {
            if (sender is TextBox input)
            {
                input.Text = input.Text.Trim();

                if (float.TryParse(input.Text, out float val))
                {
                    input.ForeColor = SystemColors.WindowText;

                    if (input == inputA) A = val;
                    else if (input == inputB) B = val;
                    else if (input == inputC) C = val;
                    else if (input == inputD) D = val;
                }
                else
                {
                    input.ForeColor = Color.Red;
                }
            }

            labelGraph.Text = "f(x) = " + Graph.ToString(Type, A, B, C, D);
        }

        public Graph GetGraph()
        {
            return new Graph(Type, A.Value, B, C, D);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            switch (Type)
            {
                case GraphType.Exponencial:
                    if (A.HasValue)
                    {
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        MessageBox.Show("Number A is invalid!");
                    }
                    break;
                case GraphType.Logarithmic:
                    if (A.HasValue)
                    {
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        MessageBox.Show("Number B is invalid!");
                    }
                    break;
                case GraphType.Quadratic:
                    if (A.HasValue && B.HasValue && C.HasValue)
                    {
                        float D = (float)Math.Pow(B.Value, 2) - 4 * (A.Value * C.Value);

                        if (D < 0)
                            MessageBox.Show("This equation cannot be solved!");
                        else
                            DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        MessageBox.Show("Invalid number detected!");
                    }
                    break;
                case GraphType.Cubic:
                    if (A.HasValue && B.HasValue && C.HasValue && D.HasValue)
                    {
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        MessageBox.Show("Invalid number detected!");
                    }
                    break;
            }
        }

        private void labelGraph_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void labelGraph_Resize(object sender, EventArgs e)
        {
            if (labelGraph.Width > 312)
                this.Width = baseWidth + (labelGraph.Width - 312);
            else
                this.Width = baseWidth;
        }
    }
}
