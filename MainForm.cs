﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gfx
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.MinimumSize = this.Size;

            canvas.MouseWheel += Canvas_MouseWheel; ;

            foreach (var item in Enum.GetNames(typeof(GraphicsQuality)))
            {
                selectQuality.Items.Add(item);
            }
            selectQuality.SelectedIndex = 1;
            //selectGraph.SelectedIndex = 0;

            GoTo(0, 0);
        }

        private void selectQuality_SelectedIndexChanged(object sender, EventArgs e)
        {
            renderQuality = (GraphicsQuality)Enum.Parse(typeof(GraphicsQuality), selectQuality.Items[selectQuality.SelectedIndex].ToString());
            canvas.Refresh();
        }

        private void selectGraph_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(selectGraph.Text))
            {
                GraphEditor editor = new GraphEditor((GraphType)selectGraph.SelectedIndex);
                DialogResult dr = editor.ShowDialog();

                if (dr == DialogResult.OK)
                {
                    Graph = editor.GetGraph();
                    labelGraph.Text = "f(x) = " + Graph.ToString();

                    canvas.Refresh();
                }
                else
                {
                    if (Graph == null)
                        selectGraph.Text = "";
                    else
                        selectGraph.SelectedIndex = (int)Graph.Type;
                }
            }
        }

        private void canvas_MouseEnter(object sender, EventArgs e)
        {
            labelCursor.Show();
        }

        private void canvas_MouseLeave(object sender, EventArgs e)
        {
            labelCursor.Hide();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.O:
                    origo = new Point(canvas.Width / 2, canvas.Height / 2);
                    canvas.Refresh();
                    break;

                case Keys.G:
                    Point c = Cursor.Position;
                    Point cp = canvas.PointToClient(c);
                    PointF gp = PointToGraph(cp);
                    GoTo(gp);
                    canvas.Refresh();
                    break;

                case Keys.F3:
                    ShowOffscreen = !ShowOffscreen;
                    canvas.Refresh();
                    break;

                default:
                    e.Handled = false;
                    break;
            }
        }

        private void connectPoints_CheckedChanged(object sender, EventArgs e)
        {
            canvas.Refresh();
        }

        private void offScreenRender_CheckedChanged(object sender, EventArgs e)
        {
            canvas.Refresh();
        }

        #region Canvas
        private const int MOUSE_WHEEL = 10;
        private const float NUMLINE_SIZE = 10;
        private const int MIN_NUMBERED_CELL_SIZE = 10;
        private const int OFFSCREEN_RENDER_EXTRA = 50;
        
        private float CELL_SIZE = 100;
        private Point origo;
        private Point? MouseDownStart = null;
        private Point lastOrigo;
        private PointF? lastGraphPoint = null;
        private GraphicsQuality renderQuality = GraphicsQuality.Normal;

        private Pen AxisPen = new Pen(Brushes.Black, 2);
        private Pen GridPen = Pens.LightGray;
        private Pen NumsPen = Pens.Black;

        private Graph Graph = null;

        private bool ShowOffscreen = false;

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            if (ShowOffscreen)
            {
                e.Graphics.PageScale = 0.5F;
                e.Graphics.TranslateTransform(canvas.Width / 4, canvas.Height / 4);
                e.Graphics.DrawRectangle(Pens.Blue, canvas.ClientRectangle);
            }

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            e.Graphics.SetGraphicsQuality(renderQuality);

            // Rácsok
            int Xoffset = origo.X % (int)CELL_SIZE;
            int Yoffset = origo.Y % (int)CELL_SIZE;

            if (!MouseDownStart.HasValue || renderQuality == GraphicsQuality.Maximum)
            {
                for (int i = 0; i < canvas.Height / CELL_SIZE + 1; i++)
                {
                    int y = (int)CELL_SIZE * i + Yoffset;
                    // Vízszintes
                    e.Graphics.DrawLine(GridPen, 0, y, canvas.Width, y);

                    // Szám jelölések
                    if (CELL_SIZE > MIN_NUMBERED_CELL_SIZE)
                    {
                        e.Graphics.DrawLine(NumsPen, (float)origo.X + NUMLINE_SIZE, (float)y, (float)origo.X - NUMLINE_SIZE, (float)y);
                        e.Graphics.DrawString((((y - origo.Y) / (int)CELL_SIZE) * -1).ToString(), this.Font, Brushes.Black, new PointF(origo.X, y));
                    }
                }
                for (int i = 0; i < canvas.Width / CELL_SIZE + 1; i++)
                {
                    int x = (int)CELL_SIZE * i + Xoffset;

                    // Függőleges
                    e.Graphics.DrawLine(GridPen, x, 0, x, canvas.Height);

                    // Szám jelölések
                    if (CELL_SIZE > MIN_NUMBERED_CELL_SIZE)
                    {
                        e.Graphics.DrawLine(NumsPen, (float)x, (float)origo.Y + NUMLINE_SIZE, (float)x, (float)origo.Y - NUMLINE_SIZE);
                        e.Graphics.DrawString(((x - origo.X) / (int)CELL_SIZE).ToString(), this.Font, Brushes.Black, new PointF(x, origo.Y));
                    }
                }
            }

            // X tengely
            e.Graphics.DrawLine(AxisPen, new Point(0, origo.Y), new Point(canvas.Width, origo.Y));

            // Y tengely
            e.Graphics.DrawLine(AxisPen, new Point(origo.X, 0), new Point(origo.X, canvas.Height));

            if (Graph != null && (!MouseDownStart.HasValue || renderQuality == GraphicsQuality.Maximum))
            {
                Pen GraphPen = new Pen(Graph.Color, 2);
                lastGraphPoint = null;

                List<(Point, float)> Xvalues = new List<(Point, float)>();
                // Grafikon rajzolás
                for (int i = 0; i < canvas.Width; i++)
                {
                    try
                    {
                        float x = (float)((float)i - (float)origo.X) / (float)CELL_SIZE;
                        float y = (float)Graph.GetValueAt((float)x);

                        if (!float.IsFinite(y))
                            continue;

                        PointF GraphPoint = GraphToPoint(new PointF(x, y));

                        RectangleF rect = new Rectangle(OFFSCREEN_RENDER_EXTRA * -1, OFFSCREEN_RENDER_EXTRA * -1, canvas.Width + (OFFSCREEN_RENDER_EXTRA * 2), canvas.Height + (OFFSCREEN_RENDER_EXTRA * 2));

                        if (connectPoints.Checked && lastGraphPoint.HasValue)
                        {
                            if (offScreenRender.Checked || (rect.Contains(lastGraphPoint.Value) || rect.Contains(GraphPoint)))
                                e.Graphics.DrawLine(GraphPen, lastGraphPoint.Value, GraphPoint);
                        }
                        else
                        {
                            e.Graphics.FillRectangle(new SolidBrush(Graph.Color), new RectangleF(GraphPoint, new SizeF(2, 2)));
                        }

                        if (lastGraphPoint.HasValue)
                        {
                            if (lastGraphPoint.Value.CreateRectangle(GraphPoint).IntersectsWith(new RectangleF(GraphPoint.X - 1, origo.Y, 10, 1)))
                            {
                                Xvalues.Add((new Point((int)GraphPoint.X, origo.Y), x));
                            }
                        }

                        if (devMode.Checked)
                        {
                            e.Graphics.FillRectangle(new SolidBrush(Color.Blue), new RectangleF(new PointF(i, canvas.Height - 4), new SizeF(1, 4)));

                            string debugStr = "Graph: " + new PointF(x, y) + "\nPos: " + GraphPoint;
                            SizeF debugSize = e.Graphics.MeasureString(debugStr, this.Font);
                            RectangleF debugBox = new RectangleF(new PointF(1, canvas.Height - debugSize.Height - 10), debugSize);
                            e.Graphics.FillRectangle(Brushes.White, debugBox);
                            e.Graphics.DrawString(debugStr, this.Font, Brushes.Black, debugBox.Location);

                            System.Threading.Thread.Sleep(1);
                        }

                        lastGraphPoint = GraphPoint;
                    }
                    catch (Exception) { break; }
                }

                if (Graph.Type == GraphType.Cubic || Graph.Type == GraphType.Quadratic)
                {
                    Rectangle lastDot = new Rectangle(-10, -10, -10, -10);
                    foreach ((Point, float) value in Xvalues)
                    {
                        Rectangle dot = new Rectangle(new Point(value.Item1.X - 5, value.Item1.Y - 5), new Size(10, 10));

                        if (!lastDot.IntersectsWith(dot))
                        {
                            e.Graphics.DrawString(Math.Round(value.Item2, 2).ToString(), this.Font, Brushes.Green, new PointF(value.Item1.X + 5, value.Item1.Y - 22));
                            e.Graphics.FillEllipse(Brushes.Green, dot);
                        }

                        lastDot = dot;
                    }
                }
            }

            stopWatch.Stop();

            e.Graphics.DrawString(stopWatch.ElapsedMilliseconds + "ms", this.Font, Brushes.Black, new PointF(1, 1));
        }

        private PointF PointToGraph(PointF pos)
        {
            return new PointF((pos.X - origo.X) / CELL_SIZE, ((pos.Y - origo.Y) * -1) / CELL_SIZE);
        }

        private PointF GraphToPoint(PointF pos)
        {
            return new PointF((pos.X * CELL_SIZE) + origo.X, ((pos.Y * CELL_SIZE) * -1) + origo.Y);
        }

        private void GoTo(PointF pos)
        {
            origo = new Point(canvas.Width / 2, canvas.Height / 2);
            origo.X -= (int)(pos.X * CELL_SIZE);
            origo.Y += (int)(pos.Y * CELL_SIZE);
        }

        private void GoTo(float x, float y)
        {
            GoTo(new PointF(x, y));
        }

        private void canvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                CELL_SIZE = 100;
                return;
            }

            if (!MouseDownStart.HasValue)
            {
                MouseDownStart = e.Location;
                lastOrigo = origo;
            }
        }

        private void canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (MouseDownStart.HasValue)
            {
                origo = new Point(lastOrigo.X - (MouseDownStart.Value.X - e.Location.X), lastOrigo.Y - (MouseDownStart.Value.Y - e.Location.Y));
                canvas.Refresh();
            }

            if (labelCursor.Visible)
            {
                Point CursorPos = canvas.PointToClient(Cursor.Position);
                PointF CursorGraphPos = PointToGraph(CursorPos);
                labelCursor.Text = "Cursor: X=" + Math.Round(CursorGraphPos.X, 1) + "; Y=" + Math.Round(CursorGraphPos.Y, 1);
            }
        }

        private void canvas_MouseUp(object sender, MouseEventArgs e)
        {
            MouseDownStart = null;
            canvas.Refresh();
        }
        
        private void Canvas_MouseWheel(object sender, MouseEventArgs e)
        {
            PointF centerBefore = PointToGraph(new PointF(canvas.Width / 2, canvas.Height / 2));

            if (e.Delta > 0)
                CELL_SIZE += MOUSE_WHEEL;
            else if (e.Delta < 0)
                CELL_SIZE -= MOUSE_WHEEL;

            if (CELL_SIZE <= 10)
                CELL_SIZE = 10;

            GoTo(centerBefore);

            canvas.Refresh();
        }
        #endregion
    }
}
