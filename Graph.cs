﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Gfx
{
    public class Graph
    {
        public GraphType Type { get; set; }
        public Color Color { get; set; } = Color.Red;

        private float? a = null;
        private float? b = null;
        private float? c = null;
        private float? d = null;

        public Graph(GraphType Type, float a, float? b = null, float? c = null, float? d = null)
        {
            this.Type = Type;

            if (((int)Type) > 1)
            {
                if (!b.HasValue || !c.HasValue)
                    throw new ArgumentNullException();
            }

            if (Type == GraphType.Cubic)
                if (!b.HasValue || !c.HasValue || !d.HasValue)
                    throw new ArgumentNullException();

            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        public float GetValueAt(float X)
        {
            switch (Type)
            {
                case GraphType.Exponencial:
                    return (float)Math.Pow(a.Value, X); // a^x
                    
                case GraphType.Logarithmic:
                    return (float)Math.Log(X, a.Value); // log[a](x)
                    
                case GraphType.Quadratic:
                    return (a.Value * (float)(Math.Pow(X, 2)) + (b.Value * c.Value) + c.Value); // ax^2 + bc + c
                    
                case GraphType.Cubic:
                    return (float)((a.Value * (float)Math.Pow(X, 3)) + (b.Value * (float)Math.Pow(X, 2)) + (c.Value * X) + d.Value); // ax^3 + bx^2 + cx + d
                    
                default:
                    return X;
            }
        }

        private static string getSubscript(string text)
        {
            return text
                .Replace('0', '₀')
                .Replace('1', '₁')
                .Replace('2', '₂')
                .Replace('3', '₃')
                .Replace('4', '₄')
                .Replace('5', '₅')
                .Replace('6', '₆')
                .Replace('7', '₇')
                .Replace('8', '₈')
                .Replace('9', '₉')
                .Replace('a', 'ₐ');
        }

        public override string ToString()
        {
            return ToString(Type, a, b, c, d);
        }

        public static string ToString(GraphType Type, float? a, float? b, float? c, float? d)
        {
            switch (Type)
            {
                case GraphType.Exponencial:
                    return (a.HasValue ? a.ToString() : "a") + "ˣ";

                case GraphType.Logarithmic:
                    return "log" + getSubscript((a.HasValue ? a.ToString() : "a")) + "(x)";

                case GraphType.Quadratic:
                    return "(" + (a.HasValue ? a.ToString() : "a") + " * x)² + (" + (b.HasValue ? b.ToString() : "b") + " * " + (c.HasValue ? c.ToString() : "c") + ") + " + (c.HasValue ? c.ToString() : "c");

                case GraphType.Cubic:
                    return "(" + (a.HasValue ? a.ToString() : "a") + " * x)³ + (" + (b.HasValue ? b.ToString() : "b") + " * x)² + (" + (c.HasValue ? c.ToString() : "c") + " * x) + " + (d.HasValue ? d.ToString() : "d");

                default:
                    return "x";
            }
        }
    }

    public enum GraphType
    {
        Exponencial,
        Logarithmic,
        Quadratic,
        Cubic
    }
}
