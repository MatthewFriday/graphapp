﻿
namespace Gfx
{
    partial class GraphEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelA = new System.Windows.Forms.Label();
            this.inputA = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.inputB = new System.Windows.Forms.TextBox();
            this.labelB = new System.Windows.Forms.Label();
            this.inputC = new System.Windows.Forms.TextBox();
            this.labelC = new System.Windows.Forms.Label();
            this.labelGraph = new System.Windows.Forms.Label();
            this.inputD = new System.Windows.Forms.TextBox();
            this.labelD = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(12, 15);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(18, 15);
            this.labelA.TabIndex = 0;
            this.labelA.Text = "A:";
            // 
            // inputA
            // 
            this.inputA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputA.Location = new System.Drawing.Point(36, 12);
            this.inputA.Name = "inputA";
            this.inputA.Size = new System.Drawing.Size(288, 23);
            this.inputA.TabIndex = 1;
            this.inputA.TextChanged += new System.EventHandler(this.ValidateInput);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(249, 180);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(168, 180);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // inputB
            // 
            this.inputB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputB.Location = new System.Drawing.Point(36, 41);
            this.inputB.Name = "inputB";
            this.inputB.Size = new System.Drawing.Size(288, 23);
            this.inputB.TabIndex = 2;
            this.inputB.TextChanged += new System.EventHandler(this.ValidateInput);
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Location = new System.Drawing.Point(12, 44);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(17, 15);
            this.labelB.TabIndex = 4;
            this.labelB.Text = "B:";
            // 
            // inputC
            // 
            this.inputC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputC.Location = new System.Drawing.Point(36, 70);
            this.inputC.Name = "inputC";
            this.inputC.Size = new System.Drawing.Size(288, 23);
            this.inputC.TabIndex = 3;
            this.inputC.TextChanged += new System.EventHandler(this.ValidateInput);
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Location = new System.Drawing.Point(12, 73);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(18, 15);
            this.labelC.TabIndex = 6;
            this.labelC.Text = "C:";
            // 
            // labelGraph
            // 
            this.labelGraph.AutoSize = true;
            this.labelGraph.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelGraph.Location = new System.Drawing.Point(12, 138);
            this.labelGraph.Name = "labelGraph";
            this.labelGraph.Size = new System.Drawing.Size(62, 30);
            this.labelGraph.TabIndex = 8;
            this.labelGraph.Text = "f(x) =";
            this.labelGraph.Click += new System.EventHandler(this.labelGraph_Click);
            this.labelGraph.Resize += new System.EventHandler(this.labelGraph_Resize);
            // 
            // inputD
            // 
            this.inputD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputD.Location = new System.Drawing.Point(36, 99);
            this.inputD.Name = "inputD";
            this.inputD.Size = new System.Drawing.Size(288, 23);
            this.inputD.TabIndex = 4;
            this.inputD.TextChanged += new System.EventHandler(this.ValidateInput);
            // 
            // labelD
            // 
            this.labelD.AutoSize = true;
            this.labelD.Location = new System.Drawing.Point(12, 102);
            this.labelD.Name = "labelD";
            this.labelD.Size = new System.Drawing.Size(18, 15);
            this.labelD.TabIndex = 10;
            this.labelD.Text = "D:";
            // 
            // GraphEditor
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(336, 215);
            this.Controls.Add(this.labelD);
            this.Controls.Add(this.inputD);
            this.Controls.Add(this.labelGraph);
            this.Controls.Add(this.inputC);
            this.Controls.Add(this.labelC);
            this.Controls.Add(this.inputB);
            this.Controls.Add(this.labelB);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.inputA);
            this.Controls.Add(this.labelA);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GraphEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Graph Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.TextBox inputA;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox inputB;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.TextBox inputC;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Label labelGraph;
        private System.Windows.Forms.TextBox inputD;
        private System.Windows.Forms.Label labelD;
    }
}